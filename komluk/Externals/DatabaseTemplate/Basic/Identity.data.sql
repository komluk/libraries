﻿-- Roles
INSERT INTO AspNetRoles(Id, Name) VALUES ('admins', 'Admins'), 
                                         ('mods',	'Moderators'), 
										 ('users',	'Users');

GO;
-- user name: admin
-- password : lf4J3KcjI63
INSERT INTO AspNetUsers ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], 
						 [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) 
						 VALUES (N'f1f5b36c-f324-4a2f-b512-82a7efe8edc1', NULL, 0, N'ABGEZGK4KNXkKdqcX1SIbck0iI5ey3qdiF8Mchp3N8prz/TMVxREcpUZK3LDifbjUQ==',
						  N'bf4ae198-f9a0-4c63-9f19-8baa154c40be', NULL, 0, 0, NULL, 0, 0, N'admin'); 

GO;
-- Admin's role
INSERT AspNetUserRoles VALUES (N'f1f5b36c-f324-4a2f-b512-82a7efe8edc1', 'admins');