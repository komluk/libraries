﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using Cache.Interfaces;

namespace Cache
{
    public class CacheProvider : ICache
    {
        private static readonly object locker = new object();
        protected static IDictionary Dummy = new Dictionary<object, object>();

        protected static IDictionary Cache
        {
            get
            {
                try
                {
                    return HttpContext.Current.Items;
                }
                catch
                {
                    return Dummy;
                }
            }
        }

        public static int Count
        {
            get { return Cache.Count; }
        }

        public static void Insert(string key, object value)
        {
            Cache[key] = value;
        }

        public static T Get<T>(string key, T defaultValue = default(T))
        {
            var item = Cache.Contains(key) ? Cache[key] : defaultValue;
            return item is T ? (T)item : defaultValue;
        }

        public static T GetOrInsert<T>(string cacheKey, Func<T> valueGetter, bool locking = true)
        {
            var result = Get<T>(cacheKey);
            if (result == null)
            {
                if (locking)
                {
                    lock (locker)
                    {
                        result = Get<T>(cacheKey); // double check
                        if (result == null)
                        {
                            result = valueGetter();
                            if (result != null)
                                Insert(cacheKey, result);
                        }
                    }
                }
                else
                {
                    result = valueGetter();
                    if (result != null)
                        Insert(cacheKey, result);
                }
            }
            return result;
        }

        public static IEnumerator GetEnumerator()
        {
            return Cache.GetEnumerator();
        }

        public static void Remove(string key)
        {
            Cache.Remove(key);
        }

        public static void ClearAll()
        {
            Cache.Clear();
        }

        #region Explicit implementation

        int ICache.Count
        {
            get { return Count; }
        }

        void ICache.Insert(string key, object value)
        {
            Insert(key, value);
        }

        public object this[string key]
        {
            get
            {
                return Get<object>(key);
            }
            set
            {
                Insert(key, value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        T ICache.Get<T>(string key, T defaultValue)
        {
            return Get<T>(key, defaultValue);
        }

        void ICache.Remove(string key)
        {
            Remove(key);
        }

        void ICache.ClearAll()
        {
            ClearAll();
        }
        #endregion Explicit implementation
    }
}
