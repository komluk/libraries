﻿using System;

namespace Cache.Interfaces
{
    public interface IWebCache : ICache
    {
        void Insert(string key, object value, DateTime absoluteExpiration);

        void Insert(string key, object value, TimeSpan expiration, bool sliding = false);

        T GetOrInsert<T>(string cacheKey, DateTime absoluteExpiration, Func<T> valueGetter, bool locking = true);

        T GetOrInsert<T>(string cacheKey, TimeSpan expiration, Func<T> valueGetter, bool sliding = false, bool locking = true, bool backup = false);
    }
}
