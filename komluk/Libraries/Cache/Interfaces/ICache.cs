﻿using System.Collections;

namespace Cache.Interfaces
{
    public interface ICache : IEnumerable
    {
        int Count { get; }

        void Insert(string key, object value);

        T Get<T>(string key, T defaultValue = default(T));

        object this[string key] { get; set; }

        void Remove(string key);

        void ClearAll();
    }
}
