﻿using System.Collections.Generic;

namespace Web.Utils
{
    public class ModelError
    {
        public string Name { get; set; }

        public IList<string> Errors { get; set; }
    }
}
