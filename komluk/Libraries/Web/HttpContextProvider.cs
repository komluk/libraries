﻿using System.Web;
using Web.Interfaces;

namespace Web
{
    public class HttpContextProvider : IHttpContextProvider
    {
        public HttpContext Context
        {
            get { return HttpContext.Current; }
        }
    }
}
