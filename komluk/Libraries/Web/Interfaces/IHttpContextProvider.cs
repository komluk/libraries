﻿using System.Web;

namespace Web.Interfaces
{
    public interface IHttpContextProvider
    {
        HttpContext Context { get; }
    }
}
