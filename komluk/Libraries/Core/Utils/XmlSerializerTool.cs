﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Core.Utils
{
    public static class XmlSerializerTool
    {
        public static string Serialize(object obj, Type type)
        {
            string serialized = string.Empty;
            StringWriter stringWriter = null;
            try
            {
                stringWriter = new StringWriter();
                XmlSerializer serializer = new XmlSerializer((type == null) ? obj.GetType() : type);
                serializer.Serialize(stringWriter, obj);
                serialized = stringWriter.ToString();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }
            finally
            {
                if (stringWriter != null)
                    stringWriter.Close();
            }
            return serialized;
        }

        public static string Serialize(object obj)
        {
            return Serialize(obj, null);
        }

        public static object Deserialize(string serialized, Type type)
        {
            object deserialized = null;
            StringReader stringReader = null;
            try
            {
                stringReader = new StringReader(serialized);
                XmlSerializer serializer = new XmlSerializer(type);
                deserialized = serializer.Deserialize(stringReader);
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc);
            }
            finally
            {
                if (stringReader != null)
                    stringReader.Close();
            }
            return deserialized;
        }

        public static T Deserialize<T>(string serialized)
        {
            T deserialized;
            StringReader stringReader = null;
            try
            {
                stringReader = new StringReader(serialized);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                deserialized = (T)serializer.Deserialize(stringReader);
            }
            finally
            {
                if (stringReader != null)
                    stringReader.Close();
            }
            return deserialized;
        }
    }
}
