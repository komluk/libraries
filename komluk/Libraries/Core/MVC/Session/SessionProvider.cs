﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.SessionState;
using Core.Cryptography;

namespace Core.MVC.Session
{
    public class SessionProvider : SessionStateStoreProviderBase
    {
        private readonly SessionStateSection _config;
        private readonly ISymmetricAlgorithm _crypto;

        public SessionProvider()
        {
            _config = ConfigurationManager.GetSection("system.web/sessionState") as SessionStateSection;
            _crypto = DependencyResolver.Current.GetService<ISymmetricAlgorithm>() ?? new SymetricAlgorithm();
        }

        public override void InitializeRequest(HttpContext context)
        { }

        public override void EndRequest(HttpContext context)
        { }

        public override SessionStateStoreData CreateNewStoreData(HttpContext context, int timeout)
        {
            var statics = SessionStateUtility.GetSessionStaticObjects(context);

            var storeData = GetItems(context, timeout);
            return new SessionStateStoreData(storeData.Items ?? new SessionStateItemCollection(), statics, timeout);
        }

        private SessionStateStoreData CreateEmptyStoreData(HttpContext context, int timeout)
        {
            var statics = SessionStateUtility.GetSessionStaticObjects(context);
            return new SessionStateStoreData(new SessionStateItemCollection(), statics, timeout);
        }

        public override void SetAndReleaseItemExclusive(HttpContext context, string id, SessionStateStoreData item, object lockId, bool newItem)
        {
            string data = Serialize(item.Items as SessionStateItemCollection);

            context.Response.SetCookie(new HttpCookie(_config.CookieName)
            {
                Expires = DateTime.UtcNow.Add(_config.Timeout),
                Value = data,
            });
        }

        private SessionStateStoreData GetSessionStoreItem(
            bool lockRecord,
            HttpContext context,
            string id,
            out bool locked,
            out TimeSpan lockAge,
            out object lockId,
            out SessionStateActions actions)
        {
            lockAge = TimeSpan.Zero;
            lockId = null;
            locked = false;
            actions = SessionStateActions.None;
            return GetItems(context, 10);
        }

        private SessionStateStoreData GetItems(HttpContext context, int timeout)
        {
            var cookie = context.Request.Cookies[_config.CookieName];
            string data = cookie == null ? null : cookie.Value;
            return Deserialize(context, data, timeout);
        }

        private string Serialize(SessionStateItemCollection items)
        {
            if (items == null)
                return String.Empty;

            using (var ms = new MemoryStream())
            {
                using (var writer = new BinaryWriter(ms))
                    items.Serialize(writer);

                var serialized = ms.ToArray();
                var encrypted = _crypto.Encrypt(serialized);
                return Convert.ToBase64String(encrypted);
            }
        }

        private SessionStateStoreData Deserialize(HttpContext context, string serializedItems, int timeout)
        {
            try
            {
                var encrypted = Convert.FromBase64String(serializedItems);
                var decrypted = _crypto.Decrypt(encrypted);

                using (var ms = new MemoryStream(decrypted))
                {
                    using (var reader = new BinaryReader(ms))
                    {
                        var items = SessionStateItemCollection.Deserialize(reader);
                        return new SessionStateStoreData(items, SessionStateUtility.GetSessionStaticObjects(context), timeout);
                    }
                }
            }
            catch
            {
                return CreateEmptyStoreData(context, timeout);
            }
        }

        public override SessionStateStoreData GetItem(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actions)
        {
            return GetSessionStoreItem(false, context, id, out locked, out lockAge, out lockId, out actions);
        }

        public override SessionStateStoreData GetItemExclusive(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actions)
        {
            return GetSessionStoreItem(true, context, id, out locked, out lockAge, out lockId, out actions);
        }

        public override void Dispose()
        { }

        public override bool SetItemExpireCallback(SessionStateItemExpireCallback expireCallback)
        {
            return true;
        }

        public override void CreateUninitializedItem(HttpContext context, string id, int timeout)
        { }

        public override void ReleaseItemExclusive(HttpContext context, string id, object lockId)
        { }

        public override void RemoveItem(HttpContext context, string id, object lockId, SessionStateStoreData item)
        { }

        public override void ResetItemTimeout(HttpContext context, string id)
        { }
    }
}
