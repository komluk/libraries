﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Core.Cryptography
{
    public class SymetricAlgorithm : ISymmetricAlgorithm
    {
        private readonly byte[] _key;
        private readonly byte[] _vector;

        public SymetricAlgorithm()
        {
            var configuration = ConfigurationManager.AppSettings;

            var base64Key = configuration["AES:Key"];
            var base64Vector = configuration["AES:Vector"];

            if (string.IsNullOrEmpty(base64Key) || string.IsNullOrEmpty(base64Vector))
            {
                configuration["AES:Key"] = base64Key = Convert.ToBase64String(GenerateEncryptionKey());
                configuration["AES:Vector"] = base64Vector = Convert.ToBase64String(GenerateEncryptionVector());
            }

            _key = Convert.FromBase64String(base64Key);
            _vector = Convert.FromBase64String(base64Vector);
        }

        public SymetricAlgorithm(string base64Key, string base64Vector)
            : this(Convert.FromBase64String(base64Key), Convert.FromBase64String(base64Vector))
        { }

        public SymetricAlgorithm(byte[] key, byte[] vector)
        {
            if (key == null)
                throw new ArgumentNullException("key");

            if (vector == null)
                throw new ArgumentNullException("vector");

            _key = key;
            _vector = vector;
        }

        static public byte[] GenerateEncryptionKey()
        {
            using (var rm = new RijndaelManaged())
            {
                rm.GenerateKey();
                return rm.Key;
            }
        }

        static public byte[] GenerateEncryptionVector()
        {
            using (var rm = new RijndaelManaged())
            {
                rm.GenerateIV();
                return rm.IV;
            }
        }

        public byte[] Encrypt(string data)
        {
            return Encrypt(Encoding.UTF8.GetBytes(data));
        }

        public byte[] Encrypt(byte[] data)
        {
            using (var rm = new RijndaelManaged())
            {
                ICryptoTransform encryptorTransform = rm.CreateEncryptor(_key, _vector);
                return TransformData(data, encryptorTransform);
            }
        }

        public byte[] Decrypt(string data)
        {
            return Decrypt(Encoding.UTF8.GetBytes(data));
        }

        public byte[] Decrypt(byte[] data)
        {
            using (var rm = new RijndaelManaged())
            {
                ICryptoTransform decryptorTransform = rm.CreateDecryptor(_key, _vector);
                return TransformData(data, decryptorTransform);
            }
        }

        protected byte[] TransformData(byte[] data, ICryptoTransform transformer)
        {
            if (data == null)
                throw new ArgumentNullException();

            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, transformer, CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    var transformed = new byte[ms.Length];
                    ms.Position = 0;
                    ms.Read(transformed, 0, transformed.Length);

                    return transformed;
                }
            }
        }
    }
}
