﻿namespace Core.Cryptography
{
    public interface ISymmetricAlgorithm
    {
        byte[] Encrypt(string data);

        byte[] Encrypt(byte[] data);

        byte[] Decrypt(string data);

        byte[] Decrypt(byte[] data);
    }
}
