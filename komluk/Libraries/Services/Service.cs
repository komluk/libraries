﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Services.Interfaces;
using Repository.Interfaces;

namespace Services
{
    public abstract class Service<T> : IService<T> where T : class
    {
        protected IRepositoryAsync<T> Repository;

        #region Ctor
        protected Service(IRepositoryAsync<T> repository)
        {
            Repository = repository;
        }
        #endregion

        #region IRepository
        public virtual IQueryable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            return Repository.Get(filter, orderBy, includeProperties);
        }

        public virtual T Find(params object[] keyValues)
        {
            return Repository.Find(keyValues);
        }

        public virtual IQueryable<T> Queryable()
        {
            return Repository.Queryable();
        }

        public virtual IQueryable<T> SelectQuery(string query, params object[] parameters)
        {
            return Repository.SelectQuery(query, parameters);
        }

        public virtual void Insert(T entity)
        {
            Repository.Insert(entity);
        }

        public virtual void InsertRange(IEnumerable<T> entities)
        {
            Repository.InsertRange(entities);
        }

        public virtual void Update(T entity)
        {
            Repository.Update(entity);
        }

        public virtual void Delete(object id)
        {
            Repository.Delete(id);
        }

        public virtual void Delete(T entity)
        {
            Repository.Delete(entity);
        }
        #endregion

        #region IRepositoryAsync
        public virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            return await Repository.GetAsync(filter, orderBy, includeProperties);
        }

        public virtual async Task<T> FindAsync(params object[] keyValues)
        {
            return await Repository.FindAsync(keyValues);
        }

        public virtual async Task<T> FindAsync(CancellationToken cancellationToken, params object[] keyValues)
        {
            return await Repository.FindAsync(cancellationToken, keyValues);
        }

        public virtual async Task InsertAsync(T entity)
        {
            await Repository.InsertAsync(entity);
        }

        public virtual async Task InsertRangeAsync(IEnumerable<T> entities)
        {
            await Repository.InsertRangeAsync(entities);
        }

        public virtual async Task UpdateAsync(T entity)
        {
            await Repository.UpdateAsync(entity);
        }

        public virtual async Task<bool> DeleteAsync(params object[] keyValues)
        {
            return await Repository.DeleteAsync(keyValues);
        }

        public virtual async Task<bool> DeleteAsync(CancellationToken cancellationToken, params object[] keyValues)
        {
            return await Repository.DeleteAsync(cancellationToken, keyValues);
        }

        public virtual void Dispose(bool disposed)
        {
            Repository.Dispose(disposed);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
