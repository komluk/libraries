﻿namespace Services.Models
{
    public abstract class ResponseCodes
    {
        public const string UnexpectedError = "UNEXPECTED_ERROR";
        public const string Success = "SUCCESS";
    }
}
