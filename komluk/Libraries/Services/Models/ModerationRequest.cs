﻿using Data.Entities;

namespace Services.Models
{
    public class ModerationRequest : Request
    {
        public int Id { get; set; }
        public string ModeratorId { get; set; }
        public NameStatus Status { get; set; }
        public string LocalizationId { get; set; }
    }
}
