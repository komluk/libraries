﻿namespace Services.Models
{
    public abstract class Response
    {
        public bool Status { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
