﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IService<T> where T : class
    {
        #region IRepository
        IQueryable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");
        T Find(params object[] keyValues);

        IQueryable<T> Queryable();
        IQueryable<T> SelectQuery(string query, params object[] parameters);

        void Insert(T entity);
        void InsertRange(IEnumerable<T> entities);

        void Update(T entity);

        void Delete(object id);
        void Delete(T entity);
        #endregion

        #region IRepositoryAsync
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");

        Task<T> FindAsync(params object[] keyValues);
        Task<T> FindAsync(CancellationToken cancellationToken, params object[] keyValues);

        Task InsertAsync(T entity);
        Task InsertRangeAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task<bool> DeleteAsync(params object[] keyValues);
        Task<bool> DeleteAsync(CancellationToken cancellationToken, params object[] keyValues);
        #endregion

        void Dispose(bool disposed);
    }
}
