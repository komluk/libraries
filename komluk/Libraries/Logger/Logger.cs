﻿using Elmah;
using System;
using System.Web;
using Logger.Interfaces;
using Web.Interfaces;

namespace Logger
{
    public class Logger : ILogger
    {
        private readonly IHttpContextProvider _httpContextProvider;

        public Logger(IHttpContextProvider httpContextProvider)
        {
            _httpContextProvider = httpContextProvider;
        }

        public void Log(string message)
        {
            HttpContext context = _httpContextProvider.Context;
            if (context != null)
            {
                Error newError = new Error(new Exception(message), (HttpContext)context);
                newError.Form.Clear();
                ErrorLog.GetDefault(context).Log(newError);
            }
        }

        public void Log(Exception e)
        {
            HttpContext context = _httpContextProvider.Context;
            if (context != null)
            {
                Error newError = new Error(e, (HttpContext)context);
                newError.Form.Clear();
                ErrorLog.GetDefault(context).Log(newError);
            }
        }

        public void Log(string message, Exception e)
        {
            HttpContext context = _httpContextProvider.Context;
            if (context != null)
            {
                Error newError = new Error(new Exception(message, e), (HttpContext)context);
                newError.Form.Clear();
                ErrorLog.GetDefault(context).Log(newError);
            }
        }
    }
}
