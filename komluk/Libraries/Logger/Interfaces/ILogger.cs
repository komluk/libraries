﻿using System;

namespace Logger.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
        void Log(Exception e);
        void Log(string message, Exception e);
    }
}
