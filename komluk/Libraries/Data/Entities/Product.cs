﻿namespace Data.Entities
{
    public class Product : BaseEntity
    {
        public int CategoryId { get; set; }
        public ProductCategory Category { get; set; }
    }
}
