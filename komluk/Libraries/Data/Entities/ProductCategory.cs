﻿namespace Data.Entities
{
    public class ProductCategory : BaseEntity
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public int TemplateId { get; set; }

        public Template Template { get; set; }
    }
}
