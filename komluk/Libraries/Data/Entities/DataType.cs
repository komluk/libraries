﻿namespace Data.Entities
{
    public class DataType : BaseEntity
    {
        public string DbType { get; set; }
        public string PropertyAlias { get; set; }
    }
}
