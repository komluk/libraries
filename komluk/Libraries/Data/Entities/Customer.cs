﻿namespace Data.Entities
{
    public class Customer : BaseEntity
    {
        public string CustomerName { get; set; }
    }
}
