﻿namespace Data.Entities
{
    public class Section : BaseEntity
    {
        public bool Published { get; set; }
    }
}
