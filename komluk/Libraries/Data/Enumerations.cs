﻿using System;

namespace Data.Entities
{
    public enum NameStatus
    {
        Whitelist = 0,
        Blacklist = 1,
        Pending = 2,
        Waiting = 3
    }

    [Flags]
    public enum OrderFlags : int
    {
        None = 0,
        Upc = 1,
        Grm = 2,
        Payment = 4,
        Prime = 8,
        Failed = 16,
        UpcRedeemFailed = 32
    }

    public enum ApplicationStepLevel
    {
        SpoonCreated = 1,
        CodesEntered = 2,
        LoggedIn = 3,
        MovieSelected = 4,
        RedemptionCompleted = 5
    }
}
